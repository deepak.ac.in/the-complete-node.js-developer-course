const chalk = require('chalk')
const yargs = require('yargs')
const note = require('./notes.js')

//customise verson of yargs
yargs.version('1.1.0')

//Create add command

yargs.command({
    command: 'add',
    describe: 'Add a new note',
    builder: {
        title: {
            describe: 'Note Title',
            demandOption: true,
            type: 'String'
        },
        body: {
            describe: 'Note Body',
            demandOption: true,
            type: 'String'
        }
    },
    handler(argv) {
        note.addNote(argv.title, argv.body)
    }
})

//Create remove cammand
yargs.command({
    command: 'remove',
    describe: 'Remove a note',
    builder: {
        title: {
            describe: 'Note Title',
            demandOption: true,
            type: 'String'
        }
    },
    handler(argv) {
        note.removeNote(argv.title)
    }
})

//Create read command
yargs.command({
    command: 'read',
    describe: 'Read a note',
    builder: {
        title: {
            describe: 'Note Title',
            demandOption: true,
            type: 'String'
        },
    },
    handler(argv) {
        note.readNote(argv.title)
    }
})

//Create list command
yargs.command({
    command: 'list',
    describe: 'List a note',
    handler() {
        note.listNotes()
    }
})

//add, remove, read, list
//console.log(yargs.argv)

yargs.parse()
