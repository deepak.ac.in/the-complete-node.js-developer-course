const chalk = require('chalk')
const fs = require('fs')

const addNote = (title, body) => {
    const notes = loadNote()
    // console.log(notes)

    const dupicatesNote = notes.find((note) => note.title === title)
    console.log(dupicatesNote)
        
    if(!dupicatesNote){
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes)
        console.log(chalk.green.inverse('new note added'))
    }else{
        console.log(chalk.red.inverse('title already exist!'))
    }
}

const removeNote = (title) => {
    const notes = loadNote()
    const len = notes.length
    const notesToKeep = notes.filter((note) => note.title !== title) //filter calls function(note) for each note of notes.json file
                                             // and ^ returns boolean....   
                                             
    if(notesToKeep.length === notes.length){
        console.log(chalk.red.inverse('No notes found!'))
    }else{
        console.log(chalk.green.inverse('Notes removed!'))
        saveNotes(notesToKeep)
    }
    //console.log(title)
}

const listNotes = () => {
    console.log(chalk.inverse('Your notes'))
    const notes = loadNote()
    notes.forEach(element => { 
        console.log(element.title); 
      }); 

}

const readNote = (title) => {
    const notes = loadNote()
    const note = notes.find((note) => note.title === title)
    
        if(note){
            console.log(chalk.inverse(note.title))
            console.log(note.body)
        }else{
            console.log(chalk.red('Note not found!'))
        }
    
}

const saveNotes = (notes)=> {
    const dataJson = JSON.stringify(notes)
    fs.writeFileSync('notes.json', dataJson)
}
const loadNote= () => {
    try{
        const dataBuffer = fs.readFileSync('notes.json')
        const data = dataBuffer.toString()
        const parsedData = JSON.parse(data)
        return parsedData
    }catch (e){
        // return []
    }   
}

module.exports = {
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote: readNote
}