const geocode = require('./utils/geocode')
const forecast = require('./utils/forecast')

 //--console.log(process.argv[2])-- //process.argv[] contains an array of 3 element....argument provided by user will resides from 3rd element of argv[] array
 
const address = process.argv[2]
if(!address){
    console.log('Please provide an address')
}else{
    geocode(address, (error, {longitude, latitude, location}) => {
        if(error){
            return console.log(error)
        }
        forecast(latitude, longitude, (error, forcatData) => {
            if(error){
                return console.log(error)
            }
                console.log(location)
                console.log(forcatData)
          })
    })
}
