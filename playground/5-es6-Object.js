//OBJECT PROPERTY SHORTHAND

const name = 'Demon'
const userAge = 30

//one way
// const user = {
//     name: name,     // property --> 'name' is same as variable(const) name 
//     age: userAge,   // property --> 'age' is not same as variable(const) userAge
//     location: 'Chennai'
// }

//another way(best practice) by using  OBJECT PROPERTY SHORTHAND
const user = {
    name,     
    age: userAge,
    location: 'Chennai'
}
console.log(user)

//OBJECT DESTRUCTURING
//------IT IS USEFUL, IF U HAVE AN OBJECT AND WANT TO ACCESS PROPERTY FROM IT

const product = {
    label: 'Red Notebook',
    price: '302',
    stoke:  '101',
    salePrice: undefined,
    // rating: 4.2
}

// const lable = product.lable
// const stoke = product.stoke

//const {label, stoke, rating} = product  //creating variable(const) and storing properties value of product
const {label: productLabel, stoke, rating = 5} = product
// console.log(lable)
console.log(productLabel)
console.log(stoke)
console.log(rating)

//OBJECT DESTRUCTURING in a function
const transaction = (type, { label, stoke} = {}) => {
    console.log(type, label, stoke)
}

transaction('order', product)
 
transaction('order') //we get error 'TypeError: Cannot destructure property `label` of 'undefined' or 'null'.'
                    //to avoid it we set our own default value to destructured element above like
                    //{ labe l, stoke} = {} above to avoid any error.