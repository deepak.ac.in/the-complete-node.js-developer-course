const https = require('https')
const url = 'https://api.darksky.net/forecast/c61763f749f1e7493f21496d9dc8039d/40,-70?units=si&lang=en'

const request = https.request(url, (response) => {
    let data = ''

    response.on('data', (chunk) => {
       
        data = data + chunk.toString() 
        
    })

    response.on('end', () => {
        const body = JSON.parse(data)
        console.log(body)  
    })
})
request.on('error', (error) => {
    console.log('An error ', error)
})
request.end()