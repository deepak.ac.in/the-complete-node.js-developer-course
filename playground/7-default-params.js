const greeter = (name, age) => {
    console.log(name);
}

greeter('Andrew') //printd Andrew
greeter() //prints default value i.e, 'undefined'

// -----We can define our oun default value----
const greeterNew = (name = 'user', age) => {
    console.log(name);
}

greeterNew('Andrew') //printd Andrew
greeterNew() //prints default value i.e, 'undefined'


